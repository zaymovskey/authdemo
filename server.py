import os
import json
import base64
import hmac
import hashlib
from typing import Optional

from fastapi import FastAPI, Form, Cookie
from fastapi.responses import Response


app = FastAPI()


def sign_data(data: str) -> str:
    """Возвращает подписанные данные"""
    return hmac.new(
        os.environ["SECRET_KEY"].encode(),
        msg=data.encode(),
        digestmod=hashlib.sha256
    ).hexdigest().upper()


def get_username_from_signed_string(username_signed: str) -> Optional[str]:
    """Возвращает username, если username соответствует электронной подписке"""
    username_base64, sign = username_signed.split(".")
    username = base64.b64decode(username_base64.encode()).decode()
    valid_sign = sign_data(username)
    if hmac.compare_digest(valid_sign, sign):
        return username


def get_users() -> dict:
    with open("users.json") as file:
        users = json.load(file)
    return users


def password_is_valid(username: str, password: str) -> bool:
    users = get_users()
    password_hash = hashlib.sha256(
        (password + os.environ["PASSWORD_SALT"]).encode()).hexdigest().lower()
    return users[username]["password"] == password_hash


def get_user_json_response(user: dict) -> Response:
    return Response(
        json.dumps({
            "success": True,
            "message": "Пользователь аутентифицирован",
            "user": user
        }),
        media_type="application/json"
    )


@app.get("/")
def index_page(username_signed: Optional[str] = Cookie(default=None)) -> Response():
    with open("templates/login.html", "r") as template:
        login_page = template.read()
    if not username_signed:
        return Response(login_page, media_type="text/html")
    valid_username = get_username_from_signed_string(username_signed)
    if not valid_username:
        response = Response(login_page, media_type="text/html")
        response.delete_cookie(key="username_signed")
        return response
    try:
        users = get_users()
        user = users[valid_username]
    except KeyError:
        response = Response(login_page, media_type="application/json")
        response.delete_cookie(key="username_signed")
        return response
    return get_user_json_response(user)


@app.post("/login")
def process_login_page(username: str = Form(...), password: str = Form(...)) -> Response():
    users = get_users()
    user = users.get(username)
    if not user or not password_is_valid(username, password):
        return Response(
            json.dumps({
                "success": False,
                "message": "Пользователь не найден"
            }),
            media_type="application/json")
    response = get_user_json_response(user)
    username_signed = f"{base64.b64encode(username.encode()).decode()}.{sign_data(username)}"
    response.set_cookie(key="username_signed", value=username_signed)
    return response


@app.get("/logout")
def logout() -> Response():
    response = Response(
        json.dumps({
            "success": True,
            "message": "Выход прошел успешно"
        }),
    )
    response.delete_cookie(key="username_signed")
    return response
